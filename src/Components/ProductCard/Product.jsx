import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Url } from '../../url';

function Product({product}) {

  return(
    <Link to={"/products/item/"+product._id}>
      <li className='flex flex-col gap-4 w-9/12'>
        <img className='w-64 object-cover h-96 max-w-xs transition duration-300 ease-in-out hover:scale-110' src={Url+ product.image} alt="product" />
        <h2 className='font-semibold text-md'>{product.title}</h2>
        <span className='font-semibold text-gray-600'> &#x20B9;{product.mrp}</span>
    </li>
    </Link>
  )
}

Product.propTypes = {
  product: PropTypes.shape({
    image: PropTypes.string.isRequired,
    title :PropTypes.string.isRequired,
    mrp :PropTypes.number.isRequired,
    _id: PropTypes.string.isRequired
  }).isRequired,
};


export default Product