import {  Outlet } from "react-router-dom";

import Header from "../Components/RootLayout/Header";
import Footer from "../Components/RootLayout/Footer";


function RootLayout() {

  return (
    <div className="relative">
    <Header />
    <main>
    <Outlet/>
    </main>
      <Footer/>
    </div>
  )
}

export default RootLayout